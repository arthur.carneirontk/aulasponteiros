# Aula de Ponteiro

* Todo ponteiro APONTA para um endereço de memoria.

---

## USO COMUM DO PONTEIRO

* Manipular strings, funções, vetores e variáveis.

### Primeiro Exemplo

``` c
int main(){
    int *p, x = 19;

    p = &x;

    printf("%p\n", *p);
    printf("%d\n", *p);

    return 0;
}
```

* No exemplo acima ("%d\n", *p) aponta para o valor de P, tendo como resultado o Nº 19.
* Já no caso de ("%p\n", *p), aponta o endereço de P.

---

### Segundo Exemplo

``` c
    int main(){

    int *p, q,x;

    x = 19;
    *p = &x;
    q = p;

    printf("%d\n",p);
    printf("%d\n", *p);
    printf("%d\n", q);
}

````

* O resultado da função acim é: "p = 19, x = 19, q = 19".

---

### Aritimética de Ponteiros

* Considere um ponteiro para inteiro **p1** com valor atual igual a __1000__.

Assuma, também, que um __inteiro__ tem 4 bytes.

* Após a expressão __p1++, p1__ conterá __1004__.
* Cada vez que __p1__ é incremententado, ele aponta para o próximo inteiro.
* O mesmo é verdade nos decrementos.
* Ou seja, o valor incrementado ou decrementado em ponteiros depende do **tamanho do tipo de dado** que eles apontam.

### Exemplo: Aritmética de Ponteiros

``` c
    int *p1, x = 10;

    p1 = &x;
    printf("p1: %p\n", p1);
    printf("&x: %p\n" &x);
    printf("x: %d\n" x);

    p1++;
    printf("p1: %p\n", p1);
    printf("&x: %p\n" &x);
    printf("x: %d\n" x);
````

---

### Mais Exemplos de Ponteiros

``` c
    int main() {
        int i, x[] = { 0, 1, 2, 3};

        printf("Endereco\t Conteudo\n");

        for(i = 0; i < 4; i++){
            printf("%p\t %d\t\n", &x[i], x[i]);
        }
        return 0;

    }
````

* O resultado da função acima é:

````text
ENDEREÇO         CONTEUDO
1000                0
1004                1
1008                2
1012                3
````

#### Mais Exemplos Parecidos

````c
    int main(){
        int x[] = {0 , 1, 2, 3};

        printf("Notacao de vetor \n");
        printf("Endereco\t Conteudo\n");
        printf("%p\t %d\t\n", &x[2], x[2]);
        printf("\n");
        printf("Notacao de vetor \n");
        printf("Endereco\t Conteudo\n");
        printf("%p\t %d\t\n", x, *x);

        return 0;
    }
````

* O resultado da função acima é:

````text
NOTACAO DE VETOR
ENDERECO        VALOR
040               0
048               2
````

````c
    int main(){
         int x[] = {0 , 1, 2, 3}, *p1;

         p1 = x;
         printf("Notacao de vetor \n");
        printf("Endereco\t Conteudo\n");
        printf("%p\t %d\t\n", &x[2], x[2]);
        printf("\n");
        printf("Notacao de vetor \n");
        printf("Endereco\t Conteudo\n");
        printf("%p\t %d\t\n", p1, *p1);

        return 0;
    }
````

* A função tem o mesmo resultado da anterior, só mudou o referenciamento de ponteiros.

---

### Novo Operador

````c
    int main(){
        printf("sizeof(int) = %d\n", sizeof(int));
        printf("sizeof(char) = %d\n", sizeof(char));
        printf("sizeof(int * ) = %d\n", sizeof(int*));
        printf("sizeof(char * ) = %d\n", sizeof(char*));
    }
````

* Resultado dessa função é:

````text
    sizeof(int) = 4
    sizeof(char) = 1
    sizeof(int *) = 8
    sizeof(char *) = 8
````

#### Outro Exemplo

````c
    int main(){
        int *x[2], y0[] = {0 ,1 }, y1[] = {2, 3};

        x[0] = y0;
        x[1] = y1;

        printf("Conteudo de x[0] = y0: %p\n", x[0)];
        printf("Conteudo de x[1] = y1: %p\n", x[1)];
        printf("Endereço do 1º e do  2º elemento do vetor: %p e %p\n", &x[0], &x[1]);
        printf("Conteudo de y0[0] e y0: %d  e %d\n", *(x[0] + 0), *(x[0] + 1));
        printf("Endereço de y0[0] e y0: %p  e %p\n", x[0] + 0,x[0]+ 1);
        printf("Conteudo de y1[1] e y0: %d  e %d\n", *(x[1] + 0), *(x[1] + 1));
        printf("Endereço de y1[1] e y0: %p  e %p\n", x[1] + 0,x[1]+ 1);
    }
````

---

### Tópicos Avançados de Ponteiros

#### Estruturas que contêm ponteiros

````cmake
#include <stdio.h>

int main() {

    struct horario {
        int *pHora, *pMinuto, *pSegundo;
    };

    struct horario hoje;
    int hora = 200;
    int minuto = 300;
    int segundo = 400;

    hoje.pHora = &hora;
    hoje.pMinuto = &minuto;
    hoje.pSegundo = &segundo;


    printf("Hora - %i\n", *hoje.pHora);
    printf("Minuto - %i\n", *hoje.pMinuto);
    printf("Segundo - %i\n", *hoje.pSegundo);

    *hoje.pSegundo = 1000;
    printf("Segundo - %i\n", *hoje.pSegundo);

    getchar();

}
````

- No Exemplo acima os ponteiros "pHora, pMinuto e pSegundo" apontam para o endereço em que se encontra a **struct horario hoje**.
    
    - No primeiro print temos:
    ````cmake
       Hora - 200
       Minuto - 300
       Segundo - 400       
    ````
    
    - No Segundo print definimos o endereço que o ponteiro ***hoje.pSegundo** aponta recebera o valor 1000, portanto o temos como resultado:
    ````cmake
      Hora - 200
      Minuto - 300
      Segundo - 400
      Segundo - 1000    
    ````
---

#### Como passar ponteiro como paramêtro de uma função

#### Primeiro caso

````cmake
    int main(){
    
        void testeVarivael (int x);
        void testePonteiro (int *pX);
        int teste = 1;
        int *pTeste = &teste;
    
        testeVariavel(teste);        
        
        printf("%i\n", teste);
        getchar();
        return 0;                                        
    
    }


    void testeVariavel(int x){
        ++x;    
    }


    void testePonteiro(int *pX) {
        ++*pX;        
    }
````

  - No caso acima o resultado será **1**, mesmo com a variavel **teste** sendo passada como paramêtro da função **testeVarivavel**, isso por que a varivavel **teste** da função principal é uma variavel independente.

#### Segundo Caso

````cmake
   int main(){
    
        void testeVarivael (int x);
        void testePonteiro (int *pX);
        int teste = 1;
        int *pTeste = &teste;
    
        testePonteiro(teste);        
        
        printf("%i\n", teste);
        getchar();
        return 0;                                        
    
    }


    void testeVariavel(int x){
        ++x;    
    }


    void testePonteiro(int *pX) {
        ++*pX;        
    }

````

   - No segundo caso é passado um **ponteiro** como paramêtro da função, este que salva o **endereço de memoria** da variavel teste, e portanto quando é chamado pela função **testePonteiro** altera o valor que se encontra no **endereço de memoria** da variavel **teste**.
   
 
 ## Alocação Dinâminca
 
 - Considere o Seguinte: 
    - Arrays são agrupamentos sequênciais de dados de um mesmo tipo na memória.
    - Um ponteiro guarda o **endereço de memoria**
    - O **nome do array** é um **ponteiro** para o primeiro elemento do array. 
    
 A linguagem C permite alocar (reservar) dinamicamnente (em tempo de execução) blocos de meórias utilizando ponteiros. A esse processo dá-se o nome de **ALOCAÇÃO DINÂMICA**.
 Com a __alocação dinâmica__ é possível transformar um **ponteiro** em um **array**.
 
 
- A Linguagem C ANSI usa apenas 4 funções para alocação dinâmica, sendo eles: 
 ````cmake
malloc
calloc
realloc
free
````
Existe também o operador **sizeof**.

---

### Função sizeof()

Alocar memoria do tipo int é diferente de alocar memmória do tipo char: 
- Tipos diferentes podem ter tamamnhos diferentes na memória.

````cmake
**char** 1 byte
**int** 4 bytes
**float** 4 bytes
**double** 8 bytes
````

O operador sizeof() retorna o número de bytes necessários para alocar **um único** elemento de um determinado tipo de dado.
**Exemplo**: 
````cmake
int x = sizeof(int);
printf("X = %d\n", x);

return 0;
````

- No caso acima temos como resposta "**X = 4**"; 

---
 
### Função malloc()

A função malloc serve para **alocar memória** durante a execução do programa.
Ela faz o **pedido de memoria** ao computador e retorna um **ponteiro** com o **endereço** do início do espaço de memoria alocado.

A função malloc() recebe por **parâmetro**:
- A quantiade de bytes a ser alocada.

e retorna

- **NULL**: no caso de erro;
- **ponteiro** para a primeira posição do array.

**Exemplo**: 

Criar um array de 50 inteiros(200 bytes)
````cmake
int  *v = malloc(200);
````
- 50 * 4(TAMANHO DE UM INTEIRO) = 200


Criar um array de 200 char(200 bytes)
````cmake
char *c = malloc(200);
````

Como podemos ver, para cada tipo de variavel, teria que utilizar um tamanho de vetor diferente. Qual seria a solução para esse problema? Utilizar a função **sizeof**( )

````cmake
int *v = (int*) malloc(50 * sizeof(int));
char *c = (char*) malloc(50 * sizeof(char));
````  

Assim sempre saberemos qual o tamanho em **bytes** do tipo de variavel que utilizaremos.

**Exemplo retorno nulo**:

````cmake
#include <stdio.h>
#include <stdlib.h>
int main() {

    
    int *p;
    p = (int *) malloc(5 * sizeof(int));
    if(p == NULL){
        printf("Erro: Sem Memoria Suficiente");
        exit(1);
    }
    
    int i;
    for(i = 0; i< 5; i++){
        printf("Digite p[%d]: ", i);
        scanf("%d", &p[i]);
    }
    
    system("pause");
    return 0;

}
```` 

Quando não for necessário mais a utilização da memória alocada com a função malloc(), é necessário que libere a memoria, e isto é feito utlizando a função free().

````cmake
#include <stdio.h>
#include <stdlib.h>
int main() {

    
    int *p;
    p = (int *) malloc(5 * sizeof(int));
    if(p == NULL){
        printf("Erro: Sem Memoria Suficiente");
        exit(1);
    }
    
    int i;
    for(i = 0; i< 5; i++){
        printf("Digite p[%d]: ", i);
        scanf("%d", &p[i]);
    }
    free(p);


    system("pause");
    return 0;

}
````
 
---

### Função calloc()

A função calloc serve para alocar a memória durante a execução do programa.
Ela faz o pedido de memória ao computador e retorna um ponteiro de com o endereço do início do espaço de memória alocado.

````cmake
void* calloc(unsigned int num, unsigned int size);

system("pause");

return 0;
````

**Funcionamento**:

A função calloc recebe por parametro:
- Número de elementos no array a serem alocados.
- Tamanho de cada elemento do array.

e retorna 

- **NULL:**no caso de erro.
- **ponteiro** para a primeria posição do array.

**Exemplo**: 

````cmake
//Cria array de 50 inteiros.
int *v = (int*) calloc(50,4);

//Cria array de 200 char's
int *c = (char*) calloc(200,1);
````

Na alocação da memória deve-se levar em conta o tamanho do tipo.

````cmake
#include <stdio.h>
#include <stdlib.h>
int main() {


    int *p;
    p = (int *) calloc(5 * sizeof(int));
    if(p == NULL){
        printf("Erro: Sem Memoria Suficiente");
        exit(1);
    }

    int i;
    for(i = 0; i< 5; i++){
        printf("Digite p[%d]: ", i);
        scanf("%d", &p[i]);
    }

     free(p); 

    system("pause");
    return 0;

}
````

Pode ter notado que a função __malloc__ e a função __calloc__ são bem parecidas. Então afinal, qual a diferença entre essas funções?

- **malloc** -  Faz **apenas** a alocação de memória.
- **calloc** - Tem a mesma função da função malloc, porem adiciona 0 a todos os bytes alocados.  

Se você precisa alocar e zerar a memória, **calloc**.

Se você precisa somente alocar a memória, **malloc**.

---

### Função realloc()

Serve para alocar ou realocar memória durante a execução do programa. Esta função faz o pedido de memória ao computadore retorna um ponteiro com o endereço do inicio do espaço de memória alocado.

````cmake

void* realloc(void* ptr, unsigned int num);

system("pause");
return 0;

````

A função realloc recebe por paramêtro:
- __Ponteiro__ para um bloco de memória já alocado.
- A quantidade de __bytes__ a ser alocada.

e retorna

- **NULL** no caso de erro.
- **ponteiro** para a primeira posição do **array**

**Exemplo**:

````cmake
int *v = (int*) malloc(200);
````

- Cria um array de 50 inteiros( 200 bytes).

````cmake
int *v = (int*) malloc(200);

v = (int*) realloc(v, 400);
````

- Aumenta a memória alocada para 100 inteiros( 400 bytes).

Se o ponteiro para o bloco de memória previamente alocado for **NULL**, a função realloc() irá alocar memória da mesma forma como a função malloc().

````cmake
p = (int * ) realloc(NULL, 50*sizeof(int)); == p (int * ) malloc(50*sizeof(int));
````
- Os comandos são equivalentes.

Assim como podemos liberar memoria com o realoc, da mesma forma que a função free().

````cmake
p = (int * ) realloc(p,0); == free(p);
````

- Os comandos são equivalentes.

Cuidado! Se não Houver memória suficiente para alocar a memória requisitada, a função realloc() retorna **NULL**.


#### Alocação de Matrizes

Para alocar uma matriz precisamos utlizar o conceito de "ponteiro para ponteiro".

````cmake
//ponteiro (1 nivel): cria um vetor

int*p = (int * ) malloc(5*sizeof(int));

/*Ponteiro para ponteiro ( 2 niveis): permite criar uma matriz*/
int **m;

/*ponteiro para ponteiro para ponteiro(3 níveis): permite criar um array de 3 dimensões*/
int ***d;
````

Em um ponteiro para ponteiro, cada nível do ponteiro permite criar uma nova dimensão no array.
````cmake
int* -> permite criar um array de int
int** -> permite criar um array de int*

int **p;
int i, j , N = 2;

p = (int **) malloc((N * sizeof(int * )));
````

##### Vetor de ponteiros

````cmake
int **p;
int i, j , N = 2;

p = (int **) malloc((N * sizeof(int * )));

for(i = 0; i < N; i++){
    p[i] = (int * ) malloc(N * sizeof(int));    
}
````

- No exemplo acima, cada ponteiro aponta para um outro vetor, tendo um vetor de ponteiros que apontam para outros vetores.
